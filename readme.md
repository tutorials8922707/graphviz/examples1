# Command examples
```sh
# to png
dot -T png -O 01_simple.gv
# to svg, specify name
dot -T svg -o other_name.svg 01_simple.gv
````
